gulp = require('gulp');
haml = require('gulp-ruby-haml');
watch = require('gulp-watch');
sync = require('browser-sync');
sass = require('gulp-sass');
cssnext = require('gulp-cssnext');

// gulp.task('haml', function () {
//   gulp.src('./dev/haml/*.haml')
//     .pipe(haml({
//       encodings: "UTF-8"
//     }))
//     .pipe(gulp.dest('./docs/'))
//     .pipe(sync.stream()) //これを足すとライブリロードに対応。
// });

gulp.task('sass', function () {
  gulp.src('dev/sass/*.scss')
    .pipe(sass())
    .on('error', function (err) {
      console.log(err.message);
    })
    .pipe(cssnext()) //prefixなどを追加
    .pipe(gulp.dest('docs/css/'))
    .pipe(sync.stream()) //これを足すとライブリロードに対応。
})

//index.html用のLivereload
gulp.task('index', function () {
  gulp.src('docs/index.html')
    .pipe(sync.stream()) //これを足すとライブリロードに対応。
})

//Browsersync
gulp.task('server', function () {
  sync.init({
    server: {
      baseDir: './docs'
    }
  });
})

//Watchタスクを別のタスクと分けて書いている
//Write dowm watch task apart from others.
gulp.task('watch', function () {
  gulp.watch('dev/sass/*.scss', ['sass']);
  gulp.watch('docs/index.html',['index']);
  // gulp.watch('dev/haml/*.haml', ['haml']);
})


gulp.task('default', ['server', 'watch'], function () {

});

/*
dev以下と、src/image以下の中が更新されたら、自動リロード


*/
